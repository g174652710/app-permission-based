package uz.pdp.apppermissionbased.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.apppermissionbased.entity.Role;
import uz.pdp.apppermissionbased.entity.enums.RoleTypeEnum;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByRoleType(RoleTypeEnum roleType);
}
