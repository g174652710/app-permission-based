package uz.pdp.apppermissionbased.payload;

import lombok.*;
import uz.pdp.apppermissionbased.utils.AppConstants;

@Getter
@RequiredArgsConstructor
@Builder
@ToString
public class TokenDTO {
    private final String refreshToken;

    private final String accessToken;

    private final String tokenType = AppConstants.AUTH_TYPE_BEARER;
}
