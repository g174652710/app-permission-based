package uz.pdp.apppermissionbased;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPermissionBasedApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppPermissionBasedApplication.class, args);
    }

}
