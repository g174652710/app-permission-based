package uz.pdp.apppermissionbased.utils;

import uz.pdp.apppermissionbased.controller.AuthController;
import uz.pdp.apppermissionbased.payload.TokenDTO;

public interface AppConstants {

    String[] OPEN_PAGES = {
            AuthController.BASE_PATH + "/**",
    };

    String AUTH_HEADER = "Authorization";
    String AUTH_TYPE_BASIC = "Basic ";
    String AUTH_TYPE_BEARER = "Bearer ";

    String BASE_PATH = "/api";
}
