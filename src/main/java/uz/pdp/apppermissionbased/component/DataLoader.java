package uz.pdp.apppermissionbased.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.apppermissionbased.entity.Role;
import uz.pdp.apppermissionbased.entity.User;
import uz.pdp.apppermissionbased.entity.enums.PermissionEnum;
import uz.pdp.apppermissionbased.entity.enums.RoleTypeEnum;
import uz.pdp.apppermissionbased.repository.RoleRepository;
import uz.pdp.apppermissionbased.repository.UserRepository;

import java.util.Objects;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlMode;

    @Override
    public void run(String... args) throws Exception {
        if (Objects.equals(ddlMode, "create")) {
            Role admin = roleRepository.save(new Role(
                    "ADMIN",
                    RoleTypeEnum.ADMIN,
                    Set.of(PermissionEnum.values())
            ));

            roleRepository.save(new Role(
                    "USER",
                    RoleTypeEnum.USER,
                    Set.of(
                            PermissionEnum.CATEGORY_LIST,
                            PermissionEnum.CATEGORY_ONE)
            ));

            userRepository.save(new User(
                    "admin@admin.com",
                    passwordEncoder.encode("Root_123"),
                    admin
            ));
        }
    }
}
