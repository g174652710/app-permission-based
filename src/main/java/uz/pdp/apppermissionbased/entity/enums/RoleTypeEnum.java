package uz.pdp.apppermissionbased.entity.enums;

public enum RoleTypeEnum {
    ADMIN,
    USER,
    OTHER
}
