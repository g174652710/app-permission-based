package uz.pdp.apppermissionbased.entity;

import jakarta.persistence.*;
import lombok.Getter;
import org.hibernate.annotations.GenericGenerator;
import uz.pdp.apppermissionbased.entity.template.AbsUUIDEntity;

import java.util.UUID;

@Entity
@Getter
public class Category extends AbsUUIDEntity {

    @Column(nullable = false)
    private String name;
}
