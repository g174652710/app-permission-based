package uz.pdp.apppermissionbased.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.apppermissionbased.entity.User;
import uz.pdp.apppermissionbased.exceptions.RestException;
import uz.pdp.apppermissionbased.payload.ApiResponse;
import uz.pdp.apppermissionbased.payload.SignDTO;
import uz.pdp.apppermissionbased.payload.TokenDTO;
import uz.pdp.apppermissionbased.repository.UserRepository;
import uz.pdp.apppermissionbased.security.UserPrincipal;

import java.util.Date;
import java.util.UUID;
import java.util.prefs.BackingStoreException;

@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;

    @Value("${app.jwt.token.access.key}")
    private String ACCESS_TOKEN_KEY;

    @Value("${app.jwt.token.access.expiration}")
    private Long ACCESS_TOKEN_EXPIRATION;

    @Value("${app.jwt.token.refresh.key}")
    private String REFRESH_TOKEN_KEY;

    @Value("${app.jwt.token.refresh.expiration}")
    private Long REFRESH_TOKEN_EXPIRATION;

    public AuthService(UserRepository userRepository,
                       @Lazy AuthenticationManager authenticationManager,
                       @Lazy PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
    }

    public ApiResponse<TokenDTO> signIn(SignDTO signDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signDTO.getUsername(),
                        signDTO.getPassword())
        );
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date accessExpirationDate = new Date(System.currentTimeMillis()
                + ACCESS_TOKEN_EXPIRATION);

        String accessToken = Jwts
                .builder()
                .signWith(SignatureAlgorithm.HS256, ACCESS_TOKEN_KEY)
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(accessExpirationDate)
                .compact();

        Date refreshExpirationDate = new Date(System.currentTimeMillis()
                + ACCESS_TOKEN_EXPIRATION);

        String refreshToken = Jwts
                .builder()
                .signWith(SignatureAlgorithm.HS256, REFRESH_TOKEN_KEY)
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(refreshExpirationDate)
                .compact();

        return ApiResponse.successResponse(
                TokenDTO.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build()
        );
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));
        return new UserPrincipal(user);
    }

    public ApiResponse<String> verificationEmail(String code) {
        User user = userRepository.findByVerificationCode(code).orElseThrow();
        if (user.isEnabled())
            throw RestException.restThrow(
                    "Bu user allaqachon active bo'lgan",
                    HttpStatus.CONFLICT);

        user.setEnabled(true);
        userRepository.save(user);
        return ApiResponse.successResponse("Muvaffaqiyatli active bo'ldi");
    }

}