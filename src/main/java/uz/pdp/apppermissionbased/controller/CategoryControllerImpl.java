package uz.pdp.apppermissionbased.controller;

import org.springframework.web.bind.annotation.RestController;
import uz.pdp.apppermissionbased.entity.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class CategoryControllerImpl implements CategoryController {
    @Override
    public String add(Category category) {
        return "OK";
    }

    @Override
    public String edit(Category category, UUID id) {
        return "OK";
    }

    @Override
    public String delete(UUID id) {
        return "OK";
    }

    @Override
    public List<Category> list() {
        return new ArrayList<>();
    }

    @Override
    public Category one(UUID id) {
        return new Category();
    }
}
